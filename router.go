package main

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/discordapp/lilliput"
	"github.com/gin-gonic/gin"
	"gopkg.in/die-net/lrucache.v0"
	"gopkg.in/gregjones/httpcache.v0"
)

var imagePathToContentType = map[string]string{
	".jpg":  "image/jpeg",
	".png":  "image/png",
	".webp": "image/webp",
}

type imageDescription struct {
	host         string
	path         string
	sizes        []int
	distFormat   []string
	srcFormat    string
	maxAge       int
	cacheControl string
	regex        *regexp.Regexp
	redirect     string
	fetcher      *imageFetcher
}

var images = []imageDescription{
	imageDescription{
		host:         "https://image.eveonline.com",
		path:         "/Alliance",
		sizes:        []int{32, 64, 128},
		distFormat:   []string{".webp", ".png"},
		srcFormat:    ".png",
		maxAge:       604800,
		cacheControl: "public, no-transform, max-age=604800",
		regex:        regexp.MustCompile("^\\d+_(32|64|128)(\\.webp|\\.png)$"),
		redirect:     "/Alliance/1_",
	},
	imageDescription{
		host:         "https://image.eveonline.com",
		path:         "/Corporation",
		sizes:        []int{32, 64, 128, 256},
		distFormat:   []string{".webp", ".png"},
		srcFormat:    ".png",
		maxAge:       86400,
		cacheControl: "public, no-transform, max-age=86400",
		regex:        regexp.MustCompile("^\\d+_(32|64|128|256)(\\.webp|\\.png)$"),
		redirect:     "/Corporation/1_",
	},
	imageDescription{
		host:         "https://image.eveonline.com",
		path:         "/Character",
		sizes:        []int{32, 64, 128, 256, 512},
		distFormat:   []string{".webp", ".jpg"},
		srcFormat:    ".jpg",
		maxAge:       7200,
		cacheControl: "public, no-transform, max-age=7200",
		regex:        regexp.MustCompile("^\\d+_(32|64|128|256|512)(\\.webp|\\.jpg)$"),
		redirect:     "/Character/1_",
	},
	imageDescription{
		host:         "https://image.eveonline.com",
		path:         "/Type",
		sizes:        []int{32, 64},
		distFormat:   []string{".webp", ".png"},
		srcFormat:    ".png",
		maxAge:       604800,
		cacheControl: "public, no-transform, max-age=604800",
		regex:        regexp.MustCompile("^\\d+_(32|64)(\\.webp|\\.png)$"),
		redirect:     "/Type/1_",
	},
	imageDescription{
		host:         "https://image.eveonline.com",
		path:         "/Render",
		sizes:        []int{32, 64, 128, 256, 512},
		distFormat:   []string{".webp", ".png"},
		srcFormat:    ".png",
		maxAge:       604800,
		cacheControl: "public, no-transform, max-age=604800",
		regex:        regexp.MustCompile("^\\d+_(32|64|128|256|512)(\\.webp|\\.png)$"),
		redirect:     "/Render/1_",
	},
}

func setupRouter(cacheMaxSize, cacheMaxAge int64) *gin.Engine {
	gin.SetMode(gin.ReleaseMode)
	r := gin.New()

	r.GET("/healthz", func(c *gin.Context) {
		c.Writer.WriteHeader(204)
	})

	r.Use(ginLogger, gin.Recovery())

	fetcher := &imageFetcher{
		client: &http.Client{
			CheckRedirect: func(_ *http.Request, _ []*http.Request) error {
				return http.ErrUseLastResponse
			},
			Timeout: 5 * time.Second,
		},
		cacheSetup: cacheMaxSize > 0 && cacheMaxAge > 0,
	}

	if cacheMaxSize > 0 && cacheMaxAge > 0 {
		fetcher.client.Transport = httpcache.NewTransport(lrucache.New(cacheMaxSize, cacheMaxAge))
	}
	for index := range images {
		images[index].fetcher = fetcher
		r.GET(images[index].path+"/:file_string", images[index].ginServeImage)
	}

	return r
}

// Modified from gin source
func ginLogger(c *gin.Context) {
	// Start timer
	start := time.Now()
	path := c.Request.URL.Path

	// Process request
	c.Next()

	// Stop timer
	end := time.Now()
	latency := end.Sub(start)

	method := c.Request.Method
	statusCode := c.Writer.Status()
	comment := c.Errors.ByType(gin.ErrorTypePrivate).String()

	fmt.Printf("[EVE-IMAGE-PROXY] | %v | %3d | %13v | %7s | %s\n%s",
		end.Format("2006/01/02 - 15:04:05"),
		statusCode,
		latency,
		method,
		path,
		comment,
	)
}

func formatImg(srcImg []byte, opts *lilliput.ImageOptions) (*bytes.Reader, error) {
	decoder, err := lilliput.NewDecoder(srcImg)
	// this error reflects very basic checks,
	// mostly just for the magic bytes of the file to match known image formats
	if err != nil {
		log.Printf("error decoding image, %s", err)
		return nil, err
	}
	defer decoder.Close()

	header, err := decoder.Header()
	// this error is much more comprehensive and reflects
	// format errors
	if err != nil {
		log.Printf("error reading image header, %s", err)
		return nil, err
	}

	ops := lilliput.NewImageOps(func() int {
		if opts.ResizeMethod == lilliput.ImageOpsNoResize {
			width, height := header.Width(), header.Height()
			if height >= width {
				return height
			}
			return width
		}
		if opts.Height >= opts.Width {
			return opts.Height
		}
		return opts.Width
	}())
	defer ops.Close()

	// resize and transcode image
	outputImg, err := ops.Transform(decoder, opts, make([]byte, 0, len(srcImg)*2))
	if err != nil {
		log.Printf("error transforming image, %s", err)
		return nil, err
	}

	return bytes.NewReader(outputImg), nil
}

func (image *imageDescription) ginServeImage(c *gin.Context) {
	c.Header("Access-Control-Allow-Origin", "*")
	match := image.regex.FindStringSubmatch(c.Param("file_string"))
	if len(match) == 0 {
		c.Writer.WriteHeader(http.StatusNotFound)
		c.Writer.WriteString("404 page not found")
		return
	}

	imgBody, status, cacheHeaders, err := image.fetcher.Get(
		image.host+image.path+"/"+strings.Trim(match[0], match[2])+image.srcFormat,
		c.Request.Header.Get("If-None-Match"),
		c.Request.Header.Get("If-Modified-Since"),
	)
	if err != nil {
		log.Printf("A error occurred while fetching img from src %s", err)
		c.Writer.WriteHeader(http.StatusInternalServerError)
		c.Writer.WriteString("Internal Server Error")
		return
	}

	if imgBody == nil {
		switch status {
		case http.StatusNotModified:
			c.Writer.WriteHeader(http.StatusNotModified)
			headers := c.Writer.Header()
			headers["Cache-Control"] = []string{image.cacheControl}
			for key, value := range cacheHeaders {
				headers[key] = value
			}
			return
		case http.StatusFound:
			http.Redirect(c.Writer, c.Request, image.redirect+match[1]+match[2], http.StatusFound)
			return
		default:
			log.Printf("Unexpected StatusCode %d", status)
			c.Writer.WriteHeader(http.StatusInternalServerError)
			c.Writer.WriteString("Internal Server Error")
			return
		}
	}

	outputImg, err := formatImg(
		imgBody.Bytes(),
		&lilliput.ImageOptions{
			FileType: match[2],
			EncodeOptions: map[int]int{
				lilliput.JpegQuality:    85,
				lilliput.PngCompression: 7,
				lilliput.WebpQuality:    85,
			},
		},
	)
	if err != nil {
		c.Writer.WriteHeader(http.StatusInternalServerError)
		c.Writer.WriteString("Internal Server Error")
		return
	}

	c.Writer.WriteHeader(http.StatusOK)
	headers := c.Writer.Header()
	headers["Content-Length"] = []string{strconv.FormatInt(int64(outputImg.Len()), 10)}
	headers["Cache-Control"] = []string{image.cacheControl}
	headers["Content-Type"] = []string{imagePathToContentType[match[2]]}
	for key, value := range cacheHeaders {
		headers[key] = value
	}
	io.Copy(c.Writer, outputImg)
}

type imageFetcher struct {
	client     *http.Client
	cacheSetup bool
}

func (f *imageFetcher) Get(url, ifNoneMatch, ifModifiedSince string) (*bytes.Buffer, int, http.Header, error) {
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, 0, nil, err
	}

	if !f.cacheSetup {
		if ifNoneMatch != "" {
			req.Header.Set("If-None-Match", ifNoneMatch)
		}
		if ifModifiedSince != "" {
			req.Header.Set("If-Modified-Since", ifModifiedSince)
		}
	}

	resp, err := f.client.Do(req)
	if err != nil {
		return nil, 0, nil, err
	}
	defer resp.Body.Close()

	var cacheHeaders = make(map[string][]string, 2)
	if respEtag := resp.Header.Get("Etag"); respEtag != "" {
		var value = make([]string, 1, 1)
		value[0] = respEtag
		cacheHeaders["Etag"] = value
	}
	if respLastModified := resp.Header.Get("Last-Modified"); respLastModified != "" {
		var value = make([]string, 1, 1)
		value[0] = respLastModified
		cacheHeaders["Last-Modified"] = value
	}

	if resp.StatusCode != http.StatusOK {
		io.Copy(ioutil.Discard, resp.Body)
		return nil, resp.StatusCode, cacheHeaders, nil
	}

	if f.cacheSetup && (ifNoneMatch != "" || ifModifiedSince != "") {
		if isCacheHit(ifNoneMatch, ifModifiedSince, resp.Header) {
			io.Copy(ioutil.Discard, resp.Body)
			return nil, http.StatusNotModified, cacheHeaders, nil
		}
	}

	imgBuffer := bytes.NewBuffer(make([]byte, 0, resp.ContentLength*2))
	imgBuffer.ReadFrom(resp.Body)
	return imgBuffer, http.StatusOK, cacheHeaders, nil
}

func isCacheHit(ifNoneMatch, ifModifiedSince string, resp http.Header) (hit bool) {
	if ifNoneMatch != "" {
		if respEtag := resp.Get("Etag"); respEtag != "" {
			return ifNoneMatch == respEtag
		}
	}
	if ifModifiedSince != "" {
		if respLastModified := resp.Get("Last-Modified"); respLastModified != "" {
			parsedLastModified, err := time.Parse(http.TimeFormat, respLastModified)
			if err != nil {
				return false
			}
			parsedIfModifiedSince, err := time.Parse(http.TimeFormat, ifModifiedSince)
			if err != nil {
				return false
			}
			return !parsedLastModified.After(parsedIfModifiedSince)
		}
	}
	return false
}
