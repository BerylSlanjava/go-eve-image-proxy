FROM golang:1.10 as builder

RUN set -ex \
	&& curl -fsSL -o /usr/local/bin/dep \
  https://github.com/golang/dep/releases/download/v0.4.1/dep-linux-amd64 \
  && chmod +x /usr/local/bin/dep

WORKDIR /go/src/go-eve-image-proxy

COPY Gopkg.lock Gopkg.toml /go/src/go-eve-image-proxy/
RUN dep ensure -vendor-only

ARG commithash

COPY . .
RUN go build --ldflags '-X main.commithash='${commithash}' -s -w -extldflags "-static"' -a -tags netgo -installsuffix netgo

FROM scratch


COPY --from=builder /etc/nsswitch.conf /etc/nsswitch.conf

COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt

#   nobody:nobody
USER 65534:65534

COPY --from=builder /go/src/go-eve-image-proxy/go-eve-image-proxy .

ENTRYPOINT ["./go-eve-image-proxy"]
