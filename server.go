package main

import (
	"context"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/coreos/go-systemd/daemon"
	"github.com/namsral/flag"
)

var (
	commithash   string
	srvAddr      string
	cacheMaxSize int64
	cacheMaxAge  int64
)

func init() {
	flags := flag.NewFlagSetWithEnvPrefix(os.Args[0], "", flag.PanicOnError)
	flags.StringVar(&srvAddr, "addr", ":8080", "addr for the server")
	flags.Int64Var(&cacheMaxSize, "cache_max_size", 25*1024*1024, "used to set the max size on image server cache")
	flags.Int64Var(&cacheMaxAge, "cache_max_age", 7200, "used to set the max age on image server cache")
	flags.ParseEnv(os.Environ())
	flags.Parse(os.Args[1:])
}

func main() {
	srv := &http.Server{
		Addr:              srvAddr,
		Handler:           setupRouter(cacheMaxSize, cacheMaxAge),
		ReadTimeout:       10 * time.Second,
		ReadHeaderTimeout: 10 * time.Second,
		WriteTimeout:      10 * time.Second,
	}

	go func() {
		// service connections
		l, err := net.Listen("tcp", srvAddr)
		if err != nil {
			log.Fatal("Listen Error:", err)
		}
		log.Print("Server Starting ", commithash)
		daemon.SdNotify(false, "READY=1")
		if err := srv.Serve(l); err != nil {
			log.Fatal("Serve Error:", err)
		}
	}()

	// Wait for interrupt signal to gracefully shutdown the server with
	// a timeout of 5 seconds.
	quitCh := make(chan os.Signal)
	signal.Notify(quitCh, os.Interrupt)
	livenessCh := getSystemDLivenessLoop()
	for {
		select {
		case <-quitCh:
			log.Println("Server Shuting Down")
			ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
			defer cancel()
			if err := srv.Shutdown(ctx); err != nil {
				log.Fatal("Server Shutdown Error:", err)
			}
			log.Println("Server Exited")
			return
		case <-livenessCh:
			daemon.SdNotify(false, "WATCHDOG=1")
			continue
		}
	}
}

func getSystemDLivenessLoop() <-chan time.Time {
	interval, err := daemon.SdWatchdogEnabled(false)
	if err != nil {
		log.Println(err)
		return nil
	}
	if interval <= 0 {
		return nil
	}
	return time.Tick(interval / 2)
}
